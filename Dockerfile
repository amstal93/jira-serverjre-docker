ARG JAVA_VERSION=8
FROM store/oracle/serverjre:${JAVA_VERSION}
ARG JIRA_VERSION=7.6.8

ENV JIRA_HOME=/data \
    JIRA_INSTALL=/app \
    JIRA_VERSION=${JIRA_VERSION}\
    SSL_HOST= \
    SSL_PORT= 

EXPOSE 8080

RUN mkdir -p ${JIRA_HOME}/conf/Catalina\
             ${JIRA_INSTALL} \
 && yum update -y \
 && yum install -y \
               gcc \
               gzip \
               libxml2-devel \
               libxslt-devel \
               libxslt \
               python-devel \
               python-setuptools \
               tar \
 && easy_install lxml \
 && curl -o /usr/local/bin/gosu -SL 'https://github.com/tianon/gosu/releases/download/1.7/gosu-amd64' \
 && chmod +x /usr/local/bin/gosu \
 && useradd -M java \
 && curl -L https://atlassian.com/software/jira/downloads/binary/atlassian-jira-software-${JIRA_VERSION}.tar.gz \
  | tar xzf - --strip 1 -C ${JIRA_INSTALL} \
 && chown -R java:java ${JIRA_INSTALL}/{conf,work,logs,temp} ${JIRA_HOME} \
 && yum remove -y \
               gcc \
               gzip \
               libxml2-devel \
               libxslt-devel \
               python-devel \
               python-setuptools \
               tar \
 && yum reinstall -y \
               filesystem \
 && yum autoremove -y \
 && yum clean all

VOLUME "${JIRA_HOME}" \
       "${JIRA_INSTALL}/logs"

COPY "docker-entrypoint.sh" \
     "docker-test.sh" \
     "xmledit.py" \
     "/"
COPY "setenv.sh" \
     "/app/bin/"

HEALTHCHECK --start-period=10m \
            --interval=1m \
            --retries=3 \
        CMD curl -f http://localhost:8080/status || \
            exit 1
ENTRYPOINT ["/docker-entrypoint.sh"]
